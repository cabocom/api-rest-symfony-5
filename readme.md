
# Api Rest con Symfony 5

Imaginemos un escenario en el que tenemos un e-commerce y debemos desarrollar una API REST para obtener los productos que serviremos a una app móvil. 

El producto, se compone de campos básicos, nombre, una descripción, el precio y el tipo de IVA, (4%, 10% y 21%).

Para ello, es necesario contar con los siguientes endpoint:

* Listado de productos. Este endpoint debe incluir un filtro por nombre del producto. Además, debe de tener paginación de los resultados del listado.
* Creación de producto: Este endpoint debe permitir añadir productos a la base de datos a usuarios autenticados. El API reconocerá una petición autenticada como una que incluya la cabecera 'Authorization' con valor 'Bearer admintoken'. La particularidad de este endpoint es que el precio enviado en el endpoint es sin impuestos, y debe calcularse y almacenarse el precio con el impuesto aplicado y sin él.


## Arquitectura

Las acciones ejecutadas por los endpoints son implementadas en un controlador (`src/Controller/ApiController.php`) 
teniendo en cuenta que todas las operaciones realizadas sobre la base de datos son realizadas
a través una clase Repository (`src/Repository/ProductRepository.php`) y que el acceso a los registros se hará a través de Doctrine.
Todo el intercambio de datos entre el cliente y el servidor se hará utilizando json.

Para implementar la seguridad en los accesos a los endpoints se utilizará el mecanismo de seguridad proporcionado por Symfony.
Es importante destacar que la funcionalidad `Guard` ha sido marcada como obsoleta desde la versión 5.3 - https://symfony.com/doc/current/security/authenticator_manager.html - La clase encargada de implementar el mecanismo de seguridad es `App\Security\ApiKeyAuthenticator`

A modo de resumen la aplicación tiene la siguiente estructura

Request -> Security -> Controller -> DB
               |            |
               V            V
            Response     Response


## Endpoints

Se han implementado los siguientes endpoints en la aplicación:

**POST /api/product** para la creación de productos

Este endpoint espera recibir un json con la siguiente estructura:

```
{
    "name": "nombre del producto",
    "description": "Descripción del producto",
    "price": precio sin impuestos,
    "tax": tipo de impuesto [4, 10, 21]
}
```


**GET /api/product** para obtener un listado de todos los productos y 
**GET /api/product/{name}** para hacer una búsqueda de productos 

En los endpoints que devuelven un listado se ha habilitado variable GET `offset` ya que los resultados
están paginados según la cantidad indicada en la constante `App\Repository\ProductRepository::PAGINATOR_PER_PAGE`.
Además el json devuelto tiene la siguiente estructura:

```
data: [array con los productos],
previous: página anterior,
next: siguiente página
```


## Instalación de la bd

La instalación de la base de datos se realiza a través de migraciones de Doctrine. Para esto hay que ejecutar el siguiente comando:

```
symfony console doctrine:migrations:migrate
```

Recuerde configurar correctamente la cadena de conexión con la bd en el fichero `.env` y de crear antes la bd manualmente en su servidor.


## Pruebas

Las pruebas se han programado utilizando Codeception - https://codeception.com - 
y para poder ejecutarlas es necesario configurar correctamente la url de la api
en el fichero `tests/api.suite.yml`

Importante, las pruebas tienen que ejecutarse sobre una bd limpia:

```
symfony console doctrine:migrations:migrate first
symfony console doctrine:migrations:migrate
```



