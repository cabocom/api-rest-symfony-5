<?php

namespace App\Controller;

use App\Library\ProductInputFilter;
use App\Entity\Product as DbProduct;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Throwable;

class ApiController extends AbstractController
{
    /**
     * @var ProductRepository $productRepository
     */
    private $productRepository;


    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }


    /**
     * @Route("/api/product", name="product.listing", methods="GET")
     * @Route("/api/product/{name}", name="product.search", methods="GET")
     * Endpoint encargado de devolver el listado de productos
     */    
    public function listing(Request $request, string $name = null): Response
    {
        $offset = max(0, $request->query->getInt("offset", 0));

        //Obtenemos los productos que tenemos que devolver
        if($name) {
            $paginator = $this->productRepository->findByName($name, $offset);
        }
        else {
            $paginator = $this->productRepository->paginator($offset);
        }
        
        //Construimos la respuesta
        $output = [
            "data" => [],
            "previous" => $offset - ProductRepository::PAGINATOR_PER_PAGE,
            "next" => min(count($paginator), $offset + ProductRepository::PAGINATOR_PER_PAGE)
        ];
        foreach($paginator as $product) {
            /** @var DbProduct $product */
            $output["data"][] = $product->toArray();
        }

        //Devolvemos el resultado
        return new JsonResponse($output, Response::HTTP_OK);
    }

    /**
     * @Route("/api/product", name="product.create", methods="POST")
     * Endpoint encargado de la creación de productos
     */
    public function create(Request $request): Response
    {
        //Recogemos los datos enviados para la creación del producto
        $data = json_decode($request->getContent(), true);

        //Validamos que los datos son correctos
        $em = $this->getDoctrine()->getManager();
        $filter = new ProductInputFilter($em);
        $filter->setData($data);
        if(!$filter->isValid()) {
            return new JsonResponse(["error" => $filter->getMessages()], Response::HTTP_BAD_REQUEST);
        }

        //Los datos son correctos, creamos el producto en la bd
        try {
            $product = $this->productRepository->create($data);
        } catch (Throwable $th) {
            //Aquí se debería hacer log del error en la persistencia de los datos en la bd en lugar de devolver el error
            return new JsonResponse(["error" => $th->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        //El producto ha sido creado en la bd, devolvemos la entidad 
        return new JsonResponse($product->toArray(), Response::HTTP_CREATED);

    }

}
