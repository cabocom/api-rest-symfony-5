<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public const PAGINATOR_PER_PAGE = 2;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    /**
     * Devuelve los registros que contienen la cadena pasada dentro del campo `name`
     * @param string $name
     * @param int $offset
     * @return Paginator
     */
    public function findByName(string $name, int $offset): Paginator
    {
        $query = $this->createQueryBuilder('p')
            ->andWhere("p.name LIKE :val")
            ->setParameter('val', "%$name%")
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(self::PAGINATOR_PER_PAGE)
            ->setFirstResult($offset)
            ->getQuery()
        ;        
        return new Paginator($query);
    }

    /**
     * Crea un nuevo registro `Product` a partir de los datos pasados
     * @param array $data [name, description, price, tax]
     * @return Product
     */
    public function create(array $data): Product
    {
        $product = new Product();
        $product->setName($data["name"]);
        $product->setDescription($data["description"]);
        $product->setPrice($data["price"]);
        $product->setTax($data["tax"]);

        $priceTax = $data["price"] + ($data["price"]*$data["tax"]/100);
        $product->setPriceTax($priceTax);

        $this->_em->persist($product);
        $this->_em->flush();
        return $product;
    }

    /**
     * Obtiene un listado de productos a través de un paginator
     * @param int $offset
     * @return Paginator
     */
    public function paginator(int $offset): Paginator
    {
        $query = $this->createQueryBuilder('p')
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(self::PAGINATOR_PER_PAGE)
            ->setFirstResult($offset)
            ->getQuery()
        ;        
        return new Paginator($query);
    }

}
