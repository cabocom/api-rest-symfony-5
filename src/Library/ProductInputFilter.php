<?php

declare(strict_types=1);

namespace App\Library;

use Laminas\InputFilter\InputFilter;
use Laminas\Validator;
use Doctrine\ORM\EntityManager;

use App\Entity\Product as DbProduct;

/**
 * Input filter para la entidad Product
 * @author Javier Gálvez <jgalvez@cabocom.com>
 * @since 2021-10-01
 */
class ProductInputFilter extends InputFilter
{
    private $taxes = [4, 10, 21];


    /** @var EntityManager */
    protected $em;
    /** @var DbProduct */
    protected $dbProduct;

    /**
     * Constructor
     * @param EntityManager $em
     * @param DbAdmin|null $admin
     */
    public function __construct(EntityManager $em, ?DbProduct $dbProduct = null)
    {
        $this->em = $em;
        $this->dbProduct = $dbProduct;

        $this->addInputName();
        $this->addInputDescription();
        $this->addInputPrice();
        $this->addInputTax();
    }

    /**
     * Add input name
     * @return void
     */
    protected function addInputName(): void
    {
        $em = $this->em;
        $currentProduct = $this->dbProduct;
        $this->add([
            'name' => 'name',
            'required' => true,
            'validators' => [
                [
                    'name' => Validator\StringLength::class,
                    'options' => [
                        'min' => 1,
                        'max' => 255,
                    ],
                ],
                [
                    'name' => Validator\Callback::class,
                    'options' => [
                        'callback' => function ($name) use ($em, $currentProduct) {
                            //Comprobamos si el nombre ya está registrado
                            $product = $em->getRepository(DbProduct::class)->findOneBy(compact('name'));
                            if(!$product) {
                                return true;
                            }
                            else if($product && !$currentProduct) {
                                return false;
                            }
                            else if ($product->getId() != $currentProduct->getId()) {
                                return false;
                            }
                            else {
                                return true;
                            }

                        },
                        'messages' => [
                            Validator\Callback::INVALID_VALUE => 'Name is registered in database',
                        ],
                    ],
                ],                
            ],
        ]);
    }

    /**
     * Add input description
     * @return void
     */
    protected function addInputDescription(): void
    {
        $this->add([
            'name' => 'description',
            'required' => true,
        ]);
    }

    /**
     * Add input price
     * @return void
     */
    protected function addInputPrice(): void
    {
        $this->add([
            'name' => 'price',
            'required' => true,
            'validators' => [
                [
                    'name' => Validator\Callback::class,
                    'options' => [
                        'callback' => function ($price) {
                            return is_float($price);
                        },
                        'messages' => [
                            Validator\Callback::INVALID_VALUE => 'Price is not a correct value',
                        ],
                    ],
                ],
            ],
        ]);
    }

    /**
     * Add input tax
     * @return void
     */
    protected function addInputTax(): void
    {
        $this->add([
            'name' => 'tax',
            'required' => true,
            'validators' => [
                [
                    'name' => Validator\Callback::class,
                    'options' => [
                        'callback' => function ($tax) {
                            return in_array($tax, $this->taxes);
                        },
                        'messages' => [
                            Validator\Callback::INVALID_VALUE => 'Tax is not a correct value',
                        ],
                    ],
                ],
            ],
        ]);
    }


}
