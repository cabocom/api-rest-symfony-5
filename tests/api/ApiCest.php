<?php
namespace App\Tests;
use App\Tests\ApiTester;

class ApiCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');        
    }

    public function noAuth(ApiTester $I)
    {
        $I->sendGet("/api/product");
        $I->seeResponseCodeIs(401);        
    }

    /**
     * Comprueba la funcionalidad de creación
     */
    public function create(ApiTester $I)
    {
        $product = $this->createProduct($I);
    }

    /**
     * Comprueba la funcionalidad de búsqueda
     */
    public function search(ApiTester $I)
    {
        //Creamos productos de prueba
        $product = $this->createProduct($I);

        //Hacemos la petición de búsqueda
        $I->haveHttpHeader('Authorization', 'Bearer 1234');
        $I->sendGet("/api/product/".$product["name"]);
        $I->seeResponseCodeIs(200);

        //Comprobamos que el producto es el mismo
        $response = json_decode($I->grabResponse(), true)["data"][0]; //Solo debería haber un elemento en está búsqueda
        $I->assertEquals($product["name"], $response["name"]);
        $I->assertEquals($product["description"], $response["description"]);
        $I->assertEquals($product["price"], $response["price"]);
        $I->assertEquals($product["tax"], $response["tax"]);
        $I->assertEquals($product["priceTax"], $response["priceTax"]);        
    }

    /**
     * Comprueba la funcionalidad de listado
     */
    public function listing(ApiTester $I)
    {
        $product = $this->createProduct($I);
        //Hacemos la petición de listado
        $I->haveHttpHeader('Authorization', 'Bearer 1224');
        $I->sendGet("/api/product?offset=2");
        $I->seeResponseCodeIs(200);
        $I->seeResponseContains($product["name"]);
    }

    /**
     * Crea un producto con datos aleatorios
     * @param ApiTester $I
     * @return array
     */
    private function createProduct(ApiTester $I): array
    {   
        //Generamos datos aleatorios para el producto
        $taxes = [4, 10, 21];
        $data = [
            "name" => "name-".time().mt_rand(100, 999),
            "description" => "description-".time().mt_rand(100, 999),
            "price" => 100 * mt_rand()/mt_getrandmax(), //el precio estará entre 0 y 100
            "tax" => $taxes[array_rand($taxes)]
        ];

        //Hacemos la petición de creación a través de la api
        $I->haveHttpHeader('Authorization', 'Bearer 1234');
        $I->sendPost("/api/product", $data);

        //Comprobamos que la respuesta es correcta
        $I->seeResponseIsJson();
        $I->seeResponseCodeIs(201);

        //Y devolvemos el resultado
        $response = json_decode($I->grabResponse(), true);
        return $response;
    }
}
